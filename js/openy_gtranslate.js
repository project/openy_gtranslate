/**
 * @file openy_gtranslate.js
 */
(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.googleTranslateSwap = {
    attach: function() {
      var placeholder = $('.openy-gtranslate-placeholder');
      var elem = $('.openy-google-translate.d-none');

      placeholder.click(function (event) {
        placeholder.addClass('d-none');
        elem.removeClass('d-none');
      });
    }
  };

  Drupal.googleTranslateElementInit = function() {
    var elem = $('.openy-google-translate').get(0);
    if (elem === undefined) {
      console.log('Placeholder for google translate widget not found.');
    }
    else {
      new google.translate.TranslateElement(
        {
          pageLanguage: drupalSettings.path.currentLanguage,
          layout: google.translate.TranslateElement.InlineLayout.VERTICAL,
        },
        elem
      );
      var removePopup = document.getElementById('goog-gt-tt');
      if (removePopup) {
        removePopup.parentNode.removeChild(removePopup);
      }
    }
  };
}(jQuery, Drupal, drupalSettings));
